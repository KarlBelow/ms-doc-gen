package kbelow.demo.poi.word;

import java.io.File;
import java.io.FileOutputStream;

import org.apache.poi.xwpf.usermodel.Borders;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;

public class WordWriter {

   public static void main(String[] args) throws Exception {
      WordWriter doc = new WordWriter();

      // Blank Document
      XWPFDocument document = new XWPFDocument();

      // Write the Document in file system
      FileOutputStream out = new FileOutputStream(new File("./tmp", "GeneratedWord.docx"));

      doc.writeParagraph(document);
      doc.writeParagraphWithBorder(document);
      doc.writeTable(document);

      document.write(out);
      out.close();
      System.out.println("GeneratedWordDocument.docx written successfully");
   }

   private void writeParagraph(XWPFDocument document) throws Exception {
      // create Paragraph
      XWPFParagraph paragraph = document.createParagraph();
      XWPFRun run = paragraph.createRun();
      run.setText("At tutorialspoint.com, we strive hard to " + "provide quality tutorials for self-learning "
            + "purpose in the domains of Academics, Information "
            + "Technology, Management and Computer Programming Languages.");
   }

   private void writeParagraphWithBorder(XWPFDocument document) throws Exception {
      // create paragraph
      XWPFParagraph paragraph = document.createParagraph();

      // Set bottom border to paragraph
      paragraph.setBorderBottom(Borders.BASIC_BLACK_DASHES);

      // Set left border to paragraph
      paragraph.setBorderLeft(Borders.BASIC_BLACK_DASHES);

      // Set right border to paragraph
      paragraph.setBorderRight(Borders.BASIC_BLACK_DASHES);

      // Set top border to paragraph
      paragraph.setBorderTop(Borders.BASIC_BLACK_DASHES);

      XWPFRun run = paragraph.createRun();
      run.setText("At tutorialspoint.com, we strive hard to " + "provide quality tutorials for self-learning "
            + "purpose in the domains of Academics, Information " + "Technology, Management and Computer Programming "
            + "Languages.");
   }

   private void writeTable(XWPFDocument document) throws Exception {
      // create table
      XWPFTable table = document.createTable();

      // create first row
      XWPFTableRow tableRowOne = table.getRow(0);
      tableRowOne.getCell(0).setText("col one, row one");
      tableRowOne.addNewTableCell().setText("col two, row one");
      tableRowOne.addNewTableCell().setText("col three, row one");

      // create second row
      XWPFTableRow tableRowTwo = table.createRow();
      tableRowTwo.getCell(0).setText("col one, row two");
      tableRowTwo.getCell(1).setText("col two, row two");
      tableRowTwo.getCell(2).setText("col three, row two");

      // create third row
      XWPFTableRow tableRowThree = table.createRow();
      tableRowThree.getCell(0).setText("col one, row three");
      tableRowThree.getCell(1).setText("col two, row three");
      tableRowThree.getCell(2).setText("col three, row three");
   }
}
